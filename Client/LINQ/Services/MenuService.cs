using System;
using System.Collections.Generic;

namespace LINQ.Services
{
    public class MenuService
    {
        public static Dictionary<string, Func<int, string>> menuWithParameter = new Dictionary<string, Func<int, string>>()
        {
            {"1", GetTaskNumberByUserIdMenu},
            {"2", GetTasksListForUserMenu},
            {"3", GetListOfFinishedTasksInCurrentYearForUser},
            {"4", GetUserProjectAndTasks}
        };

        public static Dictionary<string, Func<string>> menuWithoutParametr = new Dictionary<string, Func<string>>()
        {
            {"5", GetListOfTeamsSortedByRegistration},
            {"6", GetUsersByAlphabetWithSortedTasks},
            {"7", GetProjectAndTaskStructure}
        };

        private static string GetTaskNumberByUserIdMenu(int id)
        {
            var dictionary = MainService.GetTaskNumberByUserId(id);
            string result = "Project name | Number of tasks\n";
            foreach (var pair in dictionary)
            {
                result += ($"{pair.Key.Name}  - {pair.Value}\n");
            }
            return result;
        }
        private static string GetTasksListForUserMenu(int id)
        {
            var list = MainService.GetTasksListForUser(id);
            if (list.Count == 0)
            {
                return "List is empty!\n";
            }
            string result = "Task id | Task name | Task Description\n";
            foreach (var task in list)
            {
                result += ($"{task.Id} - {task.Name} - {task.Description}\n");
            }
            return result;
        }
        
        private static string GetListOfFinishedTasksInCurrentYearForUser(int id)
        {
            var list = MainService.GetListOfFinishedTasksInCurrentYearForUser(id);
            string result = "| Task id | Task name |\n";
            if (list.Count == 0)
            {
                return "List is empty!\n";
            }
            foreach (var task in list)
            {
                result += ($"{task.Id} - {task.Name}\n");
            }
            return result;
        }
        private static string GetListOfTeamsSortedByRegistration()
        {
            string result = "| Team id | Team name | Emails of participants |\n";
            var list = MainService.GetListOfTeamsSortedByRegistration();
            if (list.Count == 0)
            {
                return "List is empty!\n";
            }
            foreach (var value in list)
            {
                result += ($"{value.Id} - {value.Name} \n");
                value.Users.ForEach(u => result += $"\t{u.Email}\n");
            }
            return result;
        }
        private static string GetUsersByAlphabetWithSortedTasks()
        {
            string result = "| User Name | User Surname | List of tasks |\n";
            var list = MainService.GetUsersByAlphabetWithSortedTasks();
            if (list.Count == 0)
            {
                return "List is empty!\n";
            }
            foreach (var value in list)
            {
                result += $"{value.User.FirstName} - {value.User.LastName}\n";
                value.Tasks.ForEach(v => result += $"\t{v.Name}\n");
            }
            return result;
        }
        private static string GetUserProjectAndTasks(int id)
        {
            string result = "| User Email | Last Project Name | Longest Task Name | Number of not finished tasks |\n";
            var userProjectAndTasks = MainService.GetUserProjectAndTasks(id);
            if (userProjectAndTasks == null)
            {
                return "Value is null!\n";
            }
            result += $"{userProjectAndTasks.User.Email} - " +
                      $"{userProjectAndTasks.LastProject.Name} - " +
                      $"{userProjectAndTasks.LongestTask.Name} - " +
                      $"{userProjectAndTasks.NotFinishedTasks}\n";
            return result;
        }
        private static string GetProjectAndTaskStructure()
        {
            string result = "| Project name | Number of performers | Name of Longest Task Description | Name of Shortest Task Name |\n";
            var userProjectAndTasks = MainService.GetProjectAndTaskStructure();
            if (userProjectAndTasks == null)
            {
                return "Value is null!\n";
            }
            result += $"{userProjectAndTasks.Project.Name} - " +
                      $"{userProjectAndTasks.NumberOfPerformancers} - " +
                      $"{userProjectAndTasks.LongestProjectTaskByDescription.Name} - " +
                      $"{userProjectAndTasks.ShortestProjectTaskByName.Name}\n";
            return result;
        }
    }
}