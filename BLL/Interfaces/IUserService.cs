using System.Collections.Generic;
using ProjectStructure.Domain.DTO.User;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        void RegisterUser(UserCreateDTO user);
        void UpdateUser(UserUpdateDTO user);
        UserReadDTO GetUser(int id);
        IEnumerable<UserReadDTO> GetAllUsers();
        void DeleteUser(int id);
    }
}