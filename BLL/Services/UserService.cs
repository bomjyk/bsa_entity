using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.User;
using ProjectStructure.Domain.Entities;


namespace ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITaskService _taskService;
        private readonly IProjectService _projectService;

        public UserService(IMapper mapper, 
            IUnitOfWork unitOfWork, 
            ITaskService taskService, 
            IProjectService projectService)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _taskService = taskService;
            _projectService = projectService;
        }
        
        public void RegisterUser(UserCreateDTO user)
        {
            User registerUser = _mapper.Map<User>(user);
            registerUser.RegisteredAt = DateTime.Now;
            if(user.TeamId != null) {
                registerUser.Team = _unitOfWork.Teams.Get(user.TeamId.Value);
            }
            _unitOfWork.Users.Create(registerUser);
            _unitOfWork.Save();
        }

        public void UpdateUser(UserUpdateDTO user)
        {
            _unitOfWork.Users.Update(_mapper.Map<User>(user));
            _unitOfWork.Save();
        }

        public UserReadDTO GetUser(int id)
        {
            return _mapper.Map<User, UserReadDTO>(_unitOfWork.Users.Get(id));
        }

        public IEnumerable<UserReadDTO> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserReadDTO>>(_unitOfWork.Users.GetAll());
        }

        public void DeleteUser(int id)
        {
            User user = _unitOfWork.Users.Get(id);
            if(user != null)
            {
                _unitOfWork.Tasks.GetAll().Where(t => t.Performer.Id == user.Id).ToList().ForEach(t =>
                {
                    _taskService.DeleteTask(t.Id);
                });
                _unitOfWork.Projects.GetAll().Where(p => p.Author.Id == user.Id).ToList().ForEach(p =>
                {
                    _projectService.DeleteProject(p.Id);
                });
                _unitOfWork.Users.Delete(user);
                _unitOfWork.Save();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}