using System;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.LINQ;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class LinqService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly IUserRepository _userRepository;

        public LinqService(ITaskRepository taskRepository, 
            IProjectRepository projectRepository,
            IUserRepository userRepository)
        {
            _taskRepository = taskRepository;
            _projectRepository = projectRepository;
            _userRepository = userRepository;
        }
        
        public Dictionary<Project, int> GetTaskNumberByUserId(int userId)
        {
            return _projectRepository.
                GetAll().
                ToList()
                .Select(p =>
                (
                    p,
                    _taskRepository.GetAll().Where(t => t.Project.Id == p.Id).Count(t => t.Performer.Id == userId)
                ))
                .ToDictionary(
                    p => p.p,
                    p => p.Item2
                );
        }
        public List<Task> GetTasksListForUser(int userId)
        {
            const int taskNameSize = 45;
            return _taskRepository.GetAll()
                .Where(t => t.Performer.Id == userId && t.Name != null && t.Name.Length < taskNameSize)
                .ToList();
        }
        public List<FinishedTaskOfYearForUser> GetListOfFinishedTasksInCurrentYearForUser(int userId)
        {
            const int currentYear = 2021;
            return _taskRepository.GetAll()
                .Where(t => t.Performer.Id == userId && t.FinishedAt != null && t.FinishedAt.Value.Year == currentYear)
                .Select(t => new FinishedTaskOfYearForUser()
                {
                    Id = t.Id, 
                    Name =  t.Name
                })
                .ToList();
        }
        public List<TeamStructureSortedByRegistrationWithUserField> GetListOfTeamsSortedByRegistration()
        {
            const int ageLimit = 10;
            return _userRepository
                .GetAll()
                .Where(u => DateTime.Now.Year - u.BirthDay.Year > ageLimit)
                .GroupBy(u => u.Team)
                .Select(u => new TeamStructureSortedByRegistrationWithUserField()
                    {
                        Id = u.Key.Id,
                        Name = u.Key.Name,
                        Users = u
                            .Where(t => t.Team == u.Key)
                            .Select(t => t)
                            .OrderBy(u => u.RegisteredAt)
                            .ToList()
                    })
                .ToList();
        }
        public List<AlphabetUserWithTask> GetUsersByAlphabetWithSortedTasks()
        {
            return _taskRepository.GetAll()
                .GroupBy(t => t.Performer)
                .OrderBy(t => t.Key.FirstName)
                .Select(g => new AlphabetUserWithTask
                {
                    Tasks = g.Select(t => t).OrderByDescending(t => t.Name).ToList(),
                    User = g.Key
                })
                .ToList();
        }

        public UserProjectAndTasks GetUserProjectAndTasks(int userId)
        {
            var projects = _projectRepository.GetAll().ToList();
            var tasks = _taskRepository.GetAll().ToList();
            return projects
                .ToList()
                .Select(p => p.Author)
                .Where(u => u.Id == userId)
                .Select(u => new UserProjectAndTasks()
                {
                    User = u,
                    LastProject = projects
                        .ToList()
                        .Where(p => p.Author.Id == userId)
                        .OrderBy(p => p.CreatedAt)
                        .Last(),
                    TasksNumber = tasks
                        .Count(t =>
                            projects
                                .ToList()
                                .Where(p => p.Author.Id == userId)
                                .OrderBy(p => p.CreatedAt)
                                .Last().Id == t.Project.Id),
                    NotFinishedTasks = tasks
                        .Count(t => t.Performer.Id == userId && t.FinishedAt == null),
                    LongestTask = tasks
                        .Where(t => t.Performer.Id == userId)
                        .Where(t => t.FinishedAt != null)
                        .OrderBy(t => t.FinishedAt - t.CreatedAt).First()
                })
                .First();
        }

        public ProjectAndTaskStructure GetProjectAndTaskStructure()
        {
            return _projectRepository.GetAll()
                .ToList()
                .Select(p => new ProjectAndTaskStructure()
                {
                    Project = p,
                    LongestProjectTaskByDescription = _taskRepository.GetAll()
                        .ToList()
                        .OrderBy(t => t.Description).Last(),
                    ShortestProjectTaskByName = _taskRepository.GetAll()
                        .ToList()
                        .OrderByDescending(t => t.Name).Last(),
                    NumberOfPerformancers = _projectRepository.GetAll()
                        .ToList()
                        .Where(pr => pr.Id == p.Id && 
                                     (_taskRepository.GetAll().ToList().Count(t => t.Project.Id == p.Id) < 3 ||
                                      p.Description.Length > 20))
                        ?.Count() ?? 0
                })
                .First();
        }
    }
}