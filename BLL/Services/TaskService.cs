using System;
using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.DTO.Task;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        
        public void AddNewTask(TaskCreateDTO item)
        {
            Task task = _mapper.Map<Task>(item);
            task.CreatedAt = DateTime.Now;
            task.Performer = _unitOfWork.Users.Get(item.PerformerId);
            task.Project = _unitOfWork.Projects.Get(item.ProjectId);
            if (task.Performer == null || task.Performer == null) throw new Exception();
            _unitOfWork.Tasks.Create(task);
            _unitOfWork.Save();
        }

        public void UpdateTask(TaskUpdateDTO item)
        {
            _unitOfWork.Tasks.Update(_mapper.Map<Task>(item));
            _unitOfWork.Save();
        }

        public TaskReadDTO GetTask(int id)
        {
            return _mapper.Map<TaskReadDTO>(_unitOfWork.Tasks.Get(id));
        }

        public IEnumerable<TaskReadDTO> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskReadDTO>>(_unitOfWork.Tasks.GetAll());
        }

        public void DeleteTask(int id)
        {
            Task task = _unitOfWork.Tasks.Get(id);
            if (task != null)
            {
                _unitOfWork.Tasks.Delete(task);
                _unitOfWork.Save();
            }
            else
            {
                throw new Exception();
            }
            
        }
    }
}