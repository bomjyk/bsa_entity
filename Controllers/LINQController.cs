using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.BLL.Services;
using ProjectStructure.Domain.DTO.LINQ;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("taskByUserId/id")]
        public ActionResult<ICollection<(Project, int)>> GetTaskByUserId(int id)
        {
            return  Ok(JsonConvert.SerializeObject(_linqService.GetTaskNumberByUserId(id).ToArray(), Formatting.Indented, new JsonSerializerSettings()));
        }
        
        [HttpGet("tasksForUser/id")]
        public ActionResult<IEnumerable<Task>> GetTasksListForUser(int id)
        {
            return  Ok(_linqService.GetTasksListForUser(id));
        }
        
        [HttpGet("listFinishedTasks/id")]
        public ActionResult<IEnumerable<FinishedTaskOfYearForUser>> GetListOfFinishedTasksInCurrentYearForUser(int id)
        {
            return  Ok(JsonConvert.SerializeObject(_linqService.GetListOfFinishedTasksInCurrentYearForUser(id), Formatting.Indented));
        }
        [HttpGet("sortedListTeams")]
        public ActionResult<IEnumerable<TeamStructureSortedByRegistrationWithUserField>> GetListOfTeamsSortedByRegistration()
        {
            return  Ok(JsonConvert.SerializeObject(_linqService.GetListOfTeamsSortedByRegistration(), Formatting.Indented));
        }
        [HttpGet("alphabetUsersWithSortedTasks")]
        public ActionResult<IEnumerable<AlphabetUserWithTask>> GetUsersByAlphabetWithSortedTasks()
        {
            return  Ok(JsonConvert.SerializeObject(_linqService.GetUsersByAlphabetWithSortedTasks(), Formatting.Indented));
        }
        [HttpGet("getUserProjectAndTasks/id")]
        public ActionResult<UserProjectAndTasks> GetUserProjectAndTasks(int id)
        {
            return  Ok(JsonConvert.SerializeObject(_linqService.GetUserProjectAndTasks(id), Formatting.Indented));
        }
        [HttpGet("getProjectAndTaskStructure")]
        public ActionResult<ProjectAndTaskStructure> GetProjectAndTaskStructure()
        {
            return  Ok(JsonConvert.SerializeObject(_linqService.GetProjectAndTaskStructure(), Formatting.Indented));
        }
    }
}