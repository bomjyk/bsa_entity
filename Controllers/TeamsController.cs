using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Domain.DTO.Team;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet("id")]
        public ActionResult<TeamReadDTO> GetTeamById(int id)
        {
            try
            {
                return Ok(_teamService.GetTeam(id));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamReadDTO>> GetAllTeams()
        {
            return Ok(_teamService.GetAllTeams());
        }

        [HttpPost]
        public ActionResult AddTeam(TeamCreateDTO team)
        {
            _teamService.CreateTeam(team);
            return Ok();
        }

        [HttpDelete]
        public ActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        [HttpPut]
        public ActionResult UpdateTeam(TeamUpdateDTO team)
        {
            _teamService.UpdateTeam(team);
            return Ok();
        }
    }
}