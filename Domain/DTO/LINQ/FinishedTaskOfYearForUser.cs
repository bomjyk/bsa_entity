namespace ProjectStructure.Domain.DTO.LINQ
{
    public class FinishedTaskOfYearForUser
    {
        public int Id;
        #nullable enable
        public string? Name;
        #nullable disable
    }
}