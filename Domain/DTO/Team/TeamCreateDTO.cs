namespace ProjectStructure.Domain.DTO.Team
{
    public class TeamCreateDTO
    {
        #nullable enable
        public string? Name { get; set; }
        #nullable disable
    }
}