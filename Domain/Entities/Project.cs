using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.Domain.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        #nullable enable
        public string? Name { get; set; }
        public string? Description { get; set; }
        #nullable disable
        [Column("DeadlineAt")]
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}