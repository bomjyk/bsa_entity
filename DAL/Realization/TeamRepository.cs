using System.Collections.Generic;
using System.Linq;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.Domain.Entities;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.DAL.Realization
{
    public class TeamRepository : ITeamRepository
    {
        private readonly ProjectContext _db;
        public TeamRepository(ProjectContext context)
        {
            _db = context;
        }
        
        public IEnumerable<Team> GetAll()
        {
            return (IEnumerable<Team>) _db.Teams;
        }

        public Team Get(int id)
        {
            return _db.Teams.Find(id);
        }

        public void Create(Team item)
        {
            _db.Teams.Add(item);
        }

        public void Update(Team item)
        {
            var team = _db.Teams.FirstOrDefault(t => t.Id == item.Id);
            if (team != null)
            {
                team.Name = item.Name;
            }
        }

        public void Delete(Team item)
        {
            _db.Teams.Remove(item);
        }
    }
}